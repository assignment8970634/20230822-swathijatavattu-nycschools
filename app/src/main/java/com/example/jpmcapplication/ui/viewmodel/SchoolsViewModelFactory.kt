package com.example.jpmcapplication.ui.viewmodel

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.example.jpmcapplication.data.usecase.GetSchoolsUseCase

class SchoolsViewModelFactory (
    private val application: Application,
    owner: SavedStateRegistryOwner,
    private val getCountriesUseCase: GetSchoolsUseCase,
    defaultArgs: Bundle? = null
    ) :
    AbstractSavedStateViewModelFactory(owner, defaultArgs) {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(
            key: String,
            modelClass: Class<T>,
            handle: SavedStateHandle
        ): T {
            if (modelClass.isAssignableFrom(SchoolsViewModel::class.java)) {
                return SchoolsViewModel(application, getCountriesUseCase, handle) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
}
