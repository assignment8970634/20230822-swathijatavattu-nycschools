package com.example.jpmcapplication.ui

import com.example.jpmcapplication.data.model.School

interface ItemOnClickListener {
    fun onItemClick(school: School)
}