package com.example.jpmcapplication.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.jpmcapplication.R
import com.example.jpmcapplication.data.model.DetailedSchool
import com.example.jpmcapplication.databinding.FragmentDetailedSchoolBinding
import com.example.jpmcapplication.main.MyApplication
import com.example.jpmcapplication.ui.viewmodel.SchoolsViewModel
import kotlinx.coroutines.launch


class DetailedSchoolFragment : Fragment() {
    private lateinit var binding: FragmentDetailedSchoolBinding

    private lateinit var desSchoolsViewModel: SchoolsViewModel


    companion object{
        fun newInstance(dbn : String) : DetailedSchoolFragment {
            val fragemnt = DetailedSchoolFragment()
            val arguments = Bundle()
            arguments.putString("dbn", dbn)
            fragemnt.arguments = arguments

            return fragemnt
        }
    }

    private lateinit var dbn: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dbn = arguments?.getString("dbn") ?: ""
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailedSchoolBinding.inflate(inflater, container, false)

        desSchoolsViewModel =
            (requireActivity().application
                    as MyApplication).getDependencyModule().provideSchoolsViewModel(this)

        desSchoolsViewModel.fetchDesScoolData()

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                desSchoolsViewModel.desSchoolsStateFlow.collect { state ->
                    when (state) {
                        is SchoolsViewModel.DesSchoolViewState.Loading -> {
                            // Show loading state in UI
                            binding.loading.visibility = View.VISIBLE
                            binding.emptyRoot.emptyRoot.visibility = View.GONE
                        }
                        is SchoolsViewModel.DesSchoolViewState.Success -> {
                           updateUI(state.desSchool)
                            binding.loading.visibility = View.GONE
                            binding.emptyRoot.emptyRoot.visibility = View.GONE
                        }
                        is SchoolsViewModel.DesSchoolViewState.Empty -> {
                            // Handle empty state in UI
                            binding.loading.visibility = View.GONE
                            binding.emptyRoot.emptyLabel.text = getString(R.string.empty_list)
                            binding.emptyRoot.emptyRoot.visibility = View.VISIBLE
                        }
                        is SchoolsViewModel.DesSchoolViewState.Error -> {
                            // Handle error state in UI
                            binding.loading.visibility = View.GONE
                            binding.emptyRoot.emptyLabel.text = state.errorMessage
                            binding.emptyRoot.emptyRoot.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }

        return binding.root
    }

    private fun updateUI(desSchool: List<DetailedSchool>) {
        try {
            val school = desSchool.first { it.dbn == dbn }
            binding.tvSatMathAvgScore.text = "Math : ${school.num_of_sat_test_takers}"
            binding.tvSatWritingAvgScore.text = "Writing: ${school.sat_writing_avg_score}"
            binding.tvSatCriticalReadingAvgScore.text = "Critical ${school.sat_critical_reading_avg_score}"
        }catch (e: NoSuchElementException){
            binding.emptyRoot.emptyLabel.text = "No matching result found."
            binding.emptyRoot.emptyRoot.visibility = View.VISIBLE
        }

    }

}