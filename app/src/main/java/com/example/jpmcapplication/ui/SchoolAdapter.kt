package com.example.jpmcapplication.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.jpmcapplication.data.model.School
import com.example.jpmcapplication.databinding.ListOfSchoolsItemBinding

class SchoolAdapter(var itemOnClickListener : ItemOnClickListener) : RecyclerView.Adapter<SchoolAdapter.SchoolsViewHolder>() {
    private var listOfSchools: List<School> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolsViewHolder {
        val binding =
            ListOfSchoolsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolsViewHolder, position: Int) {
        holder.bindView(listOfSchools[position])
        holder.itemView.setOnClickListener{
            itemOnClickListener.onItemClick(listOfSchools[position])
        }
    }

    override fun getItemCount() = listOfSchools.size

    class SchoolsViewHolder(private val binding: ListOfSchoolsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindView(schools: School) {
            binding.tvOFSchools.text = schools.school_name
        }
    }

    fun submitList(newSchools: List<School>) {
        val diffCallback = SchoolsDiffCallback(listOfSchools, newSchools)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        listOfSchools = newSchools
        diffResult.dispatchUpdatesTo(this)
    }


    internal class SchoolsDiffCallback(
        private val oldList: List<School>,
        private val newList: List<School>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size
        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean {
            return oldList[oldPosition] == newList[newPosition]
        }

        override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
            return oldList[oldPosition] == newList[newPosition]
        }
    }
}
