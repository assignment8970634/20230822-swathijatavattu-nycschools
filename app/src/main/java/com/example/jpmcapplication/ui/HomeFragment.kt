package com.example.jpmcapplication.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.jpmcapplication.R
import com.example.jpmcapplication.data.model.School
import com.example.jpmcapplication.databinding.FragmentHomeBinding
import com.example.jpmcapplication.main.MyApplication
import com.example.jpmcapplication.ui.viewmodel.SchoolsViewModel
import kotlinx.coroutines.launch


class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private lateinit var schoolsAdapter: SchoolAdapter

    private lateinit var schoolsViewModel: SchoolsViewModel


        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View {
            binding = FragmentHomeBinding.inflate(inflater, container, false)
            binding.recyclerview.setOnClickListener {

            }

            schoolsViewModel =
                (requireActivity().application
                        as MyApplication).getDependencyModule().provideSchoolsViewModel(this)

            setUpRecyclerView()

            schoolsViewModel.fetchData()
            viewLifecycleOwner.lifecycleScope.launch {
                viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                    schoolsViewModel.schoolsStateFlow.collect { state ->
                        when (state) {
                            is SchoolsViewModel.SchoolViewState.Loading -> {
                                // Show loading state in UI
                                binding.loading.visibility = View.VISIBLE
                                binding.emptyRoot.emptyRoot.visibility = View.GONE
                            }
                            is SchoolsViewModel.SchoolViewState.Success -> {
                                schoolsAdapter.submitList(state.schools)
                                binding.loading.visibility = View.GONE
                                binding.emptyRoot.emptyRoot.visibility = View.GONE
                            }
                            is SchoolsViewModel.SchoolViewState.Empty -> {
                                // Handle empty state in UI
                                binding.loading.visibility = View.GONE
                                binding.emptyRoot.emptyLabel.text = getString(R.string.empty_list)
                                binding.emptyRoot.emptyRoot.visibility = View.VISIBLE
                            }
                            is SchoolsViewModel.SchoolViewState.Error -> {
                                // Handle error state in UI
                                binding.loading.visibility = View.GONE
                                binding.emptyRoot.emptyLabel.text = state.errorMessage
                                binding.emptyRoot.emptyRoot.visibility = View.VISIBLE
                            }
                        }
                    }
                }
            }

            return binding.root
        }
        private fun setUpRecyclerView() {
            binding.recyclerview.layoutManager = LinearLayoutManager(requireContext())
            schoolsAdapter = SchoolAdapter(object: ItemOnClickListener{
                override fun onItemClick(school: School) {
                    requireActivity().supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_holder, DetailedSchoolFragment.newInstance(school.dbn), DetailedSchoolFragment::class.simpleName)
                        .addToBackStack(null)
                        .commit()
                }

            })
            binding.recyclerview.adapter = schoolsAdapter
        }
}
