package com.example.jpmcapplication.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.jpmcapplication.data.model.DetailedSchool
import com.example.jpmcapplication.data.model.School
import com.example.jpmcapplication.data.usecase.GetSchoolsUseCase
import com.example.jpmcapplication.data.utils.ErrorHandling
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class SchoolsViewModel(
    application: Application, private val getSchoolsUseCase: GetSchoolsUseCase,
    private val savedStateHandle: SavedStateHandle
) : AndroidViewModel(application) {


    private val _schoolsStateFlow =
        MutableStateFlow<SchoolViewState>(SchoolViewState.Loading)
    val schoolsStateFlow: StateFlow<SchoolViewState> get() = _schoolsStateFlow

    private val _desSchoolsStateFlow =
        MutableStateFlow<DesSchoolViewState>(DesSchoolViewState.Loading)
    val desSchoolsStateFlow: StateFlow<DesSchoolViewState> get() = _desSchoolsStateFlow

    private val keySchoolsList = "schools_list"
    private val keyDesSchool = "des_school"


    fun fetchDesScoolData() {
        val desSavedScools = savedStateHandle.get<List<DetailedSchool>>(keyDesSchool)
        if (desSavedScools != null) {
            _desSchoolsStateFlow.value = DesSchoolViewState.Success(desSavedScools)
        } else {
            viewModelScope.launch {
                getSchoolsUseCase.fetchSchoolDetails()
                    .flowOn(Dispatchers.IO)
                    .onStart { _desSchoolsStateFlow.value = DesSchoolViewState.Loading }
                    .catch { exception ->
                        _desSchoolsStateFlow.value =
                            DesSchoolViewState.Error(
                                ErrorHandling.getErrorMessage(
                                    getApplication(),
                                    exception
                                )
                            )
                    }
                    .collect { desschools ->
                        if (desschools.isEmpty()) {
                            _desSchoolsStateFlow.value = DesSchoolViewState.Empty
                        } else {
                            savedStateHandle.set(keyDesSchool, desSavedScools)
                            _desSchoolsStateFlow.value = DesSchoolViewState.Success(desschools)
                        }
                    }
            }
        }
    }

     fun fetchData() {
        val savedScools = savedStateHandle.get<List<School>>(keySchoolsList)
        if (savedScools != null) {
            _schoolsStateFlow.value = SchoolViewState.Success(savedScools)
        } else {
            viewModelScope.launch {
                getSchoolsUseCase()
                    .flowOn(Dispatchers.IO)
                    .onStart { _schoolsStateFlow.value = SchoolViewState.Loading }
                    .catch { exception ->
                        _schoolsStateFlow.value =
                            SchoolViewState.Error(
                                ErrorHandling.getErrorMessage(
                                    getApplication(),
                                    exception
                                )
                            )
                    }
                    .collect { schools ->
                        if (schools.isEmpty()) {
                            _schoolsStateFlow.value = SchoolViewState.Empty
                        } else {
                            savedStateHandle.set(keySchoolsList, savedScools)
                            _schoolsStateFlow.value = SchoolViewState.Success(schools)
                        }
                    }
            }
        }
    }

    sealed class SchoolViewState {
        object Loading : SchoolViewState()
        data class Success(val schools: List<School>) : SchoolViewState()
        object Empty : SchoolViewState()
        data class Error(val errorMessage: String) : SchoolViewState()
    }

    sealed class DesSchoolViewState {
        object Loading : DesSchoolViewState()
        data class Success(val desSchool: List<DetailedSchool>) : DesSchoolViewState()
        object Empty : DesSchoolViewState()
        data class Error(val errorMessage: String) : DesSchoolViewState()
    }
}