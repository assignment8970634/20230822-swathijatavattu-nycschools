package com.example.jpmcapplication.di

import android.app.Application
import androidx.savedstate.SavedStateRegistryOwner
import com.example.jpmcapplication.ui.viewmodel.SchoolsViewModel
import com.example.jpmcapplication.data.remote.SchoolRepository
import com.example.jpmcapplication.data.rest.ApiClient
import com.example.jpmcapplication.data.usecase.GetSchoolsUseCase
import com.example.jpmcapplication.ui.viewmodel.SchoolsViewModelFactory

class AppContainer (private val application: Application) {
    private lateinit var schoolsViewModel: SchoolsViewModel

    private val apiService = ApiClient.apiService

    private val SchoolRepository = SchoolRepository(apiService)

    private val getSchoolsUseCase = GetSchoolsUseCase(SchoolRepository)

    fun provideSchoolsViewModel(savedStateRegistryOwner: SavedStateRegistryOwner): SchoolsViewModel {
        if (this::schoolsViewModel.isInitialized) return schoolsViewModel

        schoolsViewModel = SchoolsViewModelFactory(
            application,
            savedStateRegistryOwner,
            getSchoolsUseCase
        ).create(SchoolsViewModel::class.java)
        return schoolsViewModel
    }
}