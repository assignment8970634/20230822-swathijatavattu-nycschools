package com.example.jpmcapplication.data.rest

import com.example.jpmcapplication.data.model.DetailedSchool
import com.example.jpmcapplication.data.model.School
import retrofit2.http.GET

interface ApiService {
    @GET("s3k6-pzi2.json")
    suspend fun getSchools(): List<School>

    @GET("f9bf-2cp4.json")
    suspend fun getDetailedSchoolDescription(): List<DetailedSchool>

}