package com.example.jpmcapplication.data.remote

import com.example.jpmcapplication.data.model.DetailedSchool
import com.example.jpmcapplication.data.model.School
import com.example.jpmcapplication.data.rest.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext

class SchoolRepository(private val apiService: ApiService) {
    fun getListOfSchools(): Flow<List<School>> = flow {
        val schools = withContext(Dispatchers.IO) {
            apiService.getSchools()
        }
        emit(schools)
    }
    fun getSchoolDes(): Flow<List<DetailedSchool>> = flow {
        val schoolDes = withContext(Dispatchers.IO) {
            apiService.getDetailedSchoolDescription()
        }
        emit(schoolDes)
    }
}
