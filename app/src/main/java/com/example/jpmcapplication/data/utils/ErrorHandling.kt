package com.example.jpmcapplication.data.utils

import android.content.Context
import com.example.jpmcapplication.R
import retrofit2.HttpException
import java.io.IOException
import java.net.UnknownHostException

object ErrorHandling {
    fun getErrorMessage(context: Context, throwable: Throwable): String {
        return when (throwable) {
            is UnknownHostException,
            is IOException -> context.getString(R.string.network_error_message)
            is HttpException -> context.getString(
                R.string.server_error_message,
                throwable.code(),
                throwable.message()
            )
            else -> context.getString(R.string.unknown_error_message, throwable.message)
        }
    }
}