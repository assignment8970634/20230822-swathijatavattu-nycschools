package com.example.jpmcapplication.data.usecase

import com.example.jpmcapplication.data.model.DetailedSchool
import com.example.jpmcapplication.data.model.School
import com.example.jpmcapplication.data.remote.SchoolRepository
import kotlinx.coroutines.flow.Flow

class GetSchoolsUseCase(private val schoolsRepository: SchoolRepository) {
    operator fun invoke(): Flow<List<School>> = schoolsRepository.getListOfSchools()

     fun fetchSchoolDetails(): Flow<List<DetailedSchool>> = schoolsRepository.getSchoolDes()

}