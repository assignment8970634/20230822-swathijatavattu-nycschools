package com.example.jpmcapplication.main

import android.app.Application
import com.example.jpmcapplication.di.AppContainer

class MyApplication : Application() {
    private lateinit var appContainer: AppContainer

    override fun onCreate() {
        super.onCreate()
        appContainer = AppContainer(this)
    }

    fun getDependencyModule(): AppContainer = appContainer
}